package br.com.alura.agenda.modelo;

import java.io.Serializable;

/**
 * Created by gabrielbmoro on 04/03/17.
 */

public class Aluno implements Serializable {

    private Long id;
    private String nome;
    private String site;
    private Integer nota;
    private String endereco;
    private String telefone;

    @Override
    public String toString(){
        return id + "- " + nome;
    }

    public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Integer getNota() {
        return nota;
    }

    public void setNota(Integer nota) {
        this.nota = nota;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

}
