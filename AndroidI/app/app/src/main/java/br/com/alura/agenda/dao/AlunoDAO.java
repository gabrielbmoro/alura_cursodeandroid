package br.com.alura.agenda.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.alura.agenda.modelo.Aluno;

/**
 * Created by gabrielbmoro on 04/03/17.
 */

public class AlunoDAO extends SQLiteOpenHelper implements DAO{

    private static final String TABLE_NAME ="Alunos";

    public AlunoDAO(Context context) {
        super(context, DAO.DATABASE_NAME, null, 4);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE "+ TABLE_NAME + " (id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT NOT NULL, endereco TEXT, telefone TEXT, site TEXT, nota INTEGER);";
        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS " + TABLE_NAME;
        db.execSQL(sql);
        onCreate(db);
    }

    @Override
    public void insere(Serializable object) {

        Aluno aluno = null;

        if(object instanceof Aluno) {
            aluno = (Aluno) object;

            SQLiteDatabase db = getWritableDatabase();

            ContentValues dados = pegaDadosDoAluno(aluno);

            db.insert(TABLE_NAME, null, dados);
        }
    }

    @Override
    public List busca() {
        List<Aluno> alunos = new ArrayList<Aluno>();

        String sql = "SELECT * FROM " + TABLE_NAME + ";";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);

        Aluno alunoTmp = null;

        while(c.moveToNext()) {

            alunoTmp = new Aluno();
            alunoTmp.setId(c.getLong(c.getColumnIndex("id")));
            alunoTmp.setNome(c.getString(c.getColumnIndex("nome")));
            alunoTmp.setEndereco(c.getString(c.getColumnIndex("endereco")));
            alunoTmp.setTelefone(c.getString(c.getColumnIndex("telefone")));
            alunoTmp.setSite(c.getString(c.getColumnIndex("site")));
            alunoTmp.setNota(c.getInt(c.getColumnIndex("nota")));

            alunos.add(alunoTmp);
        }

        c.close();

        return alunos;
    }

    @Override
    public void deletar(Serializable object) {
        Aluno aluno = null;

        if(object instanceof Aluno) {
            aluno = (Aluno) object;

            String params[] = {String.valueOf(aluno.getId())};

            SQLiteDatabase db = getWritableDatabase();
            db.delete(TABLE_NAME,"id = ?", params);

        }
    }

    @Override
    public void altera(Serializable object) {

        Aluno aluno = null;

        if(object instanceof Aluno) {
            aluno = (Aluno) object;

            SQLiteDatabase db = getWritableDatabase();

            ContentValues dados = pegaDadosDoAluno(aluno);

            String params[] = {String.valueOf(aluno.getId())};

            db.update(TABLE_NAME, dados, "id = ?",params);
        }
    }

    @NonNull
    private ContentValues pegaDadosDoAluno(Aluno aluno) {
        ContentValues dados = new ContentValues();
        dados.put("nome", aluno.getNome());
        dados.put("endereco", aluno.getEndereco());
        dados.put("telefone", aluno.getTelefone());
        dados.put("site", aluno.getSite());
        dados.put("nota", aluno.getNota());
        return dados;
    }

}
