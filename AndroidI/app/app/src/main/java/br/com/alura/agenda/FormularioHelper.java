package br.com.alura.agenda;

import android.widget.EditText;
import android.widget.RatingBar;

import br.com.alura.agenda.modelo.Aluno;

/**
 * Created by gabrielbmoro on 04/03/17.
 */

public class FormularioHelper {

    private Aluno aluno;
    private EditText editNome;
    private EditText editEndereco;
    private EditText editSite;
    private EditText editTelefone;
    private RatingBar ratingBar_notaDoAluno;


    public FormularioHelper(FormularioActivity activity){
        aluno = new Aluno();
        editNome = (EditText) activity.findViewById(R.id.edit_nome);
        editEndereco = (EditText) activity.findViewById(R.id.edit_endereco);
        editSite = (EditText) activity.findViewById(R.id.edit_site);
        editTelefone = (EditText) activity.findViewById(R.id.edit_telefone);
        ratingBar_notaDoAluno = (RatingBar) activity.findViewById(R.id.rating_nota);
    }


    public Aluno getAluno(){
        aluno.setNome(editNome.getText().toString());
        aluno.setEndereco(editEndereco.getText().toString());
        aluno.setSite(editSite.getText().toString());
        aluno.setTelefone(editTelefone.getText().toString());
        aluno.setNota(ratingBar_notaDoAluno.getProgress());
        return aluno;
    }

    public void alimentaFormulario(Aluno alunoParam) {
        editNome.setText(alunoParam.getNome());
        editEndereco.setText(alunoParam.getEndereco());
        editSite.setText(alunoParam.getSite());
        editTelefone.setText(alunoParam.getTelefone());
        ratingBar_notaDoAluno.setProgress(alunoParam.getNota().intValue());
        this.aluno = alunoParam;
    }


}
