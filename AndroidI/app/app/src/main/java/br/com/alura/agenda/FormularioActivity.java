package br.com.alura.agenda;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import br.com.alura.agenda.dao.AlunoDAO;
import br.com.alura.agenda.modelo.Aluno;

public class FormularioActivity extends AppCompatActivity {

    private FormularioHelper form_helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        this.form_helper = new FormularioHelper(FormularioActivity.this);

        if(getIntent().getExtras()!=null) {
            Aluno aluno = (Aluno) getIntent().getExtras().getSerializable("aluno");
            if(aluno!=null) {
                this.form_helper.alimentaFormulario(aluno);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_formulario, menu);//Qual xml vamos inflar

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_formulario_ok:

                Aluno aluno = form_helper.getAluno();

                AlunoDAO dao = new AlunoDAO(this);

                if(aluno.getId()==null) {
                    dao.insere(aluno);
                    Toast.makeText(FormularioActivity.this, "O aluno(a) " + aluno.getNome() + " foi cadastrado com sucesso!", Toast.LENGTH_SHORT).show();
                }else{
                    dao.altera(aluno);
                    Toast.makeText(FormularioActivity.this, "O aluno(a) " + aluno.getNome() + " foi atualizado com sucesso!", Toast.LENGTH_SHORT).show();
                }

                dao.close();

                finish();//não volta, evita pilha de activities
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
