# README #

- Esse repositório possui todos os arquivos utilizados por mim nos cursos de Android na plataforma de aprendizagem Alura.

- O repositório está organizado por pastas respectivas de cada curso realizado.

- Plataforma de ensino: https://cursos.alura.com.br/

### O que está nesse repositório? ###

* Aplicações de Exemplo
* APIs
* Materiais sobre desenvolvimento para Android