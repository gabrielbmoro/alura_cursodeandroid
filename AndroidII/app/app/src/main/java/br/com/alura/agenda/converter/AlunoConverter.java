package br.com.alura.agenda.converter;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONStringer;

import java.util.List;

import br.com.alura.agenda.modelo.Aluno;

/**
 * Created by gabriel-moro on 07/03/17.
 */
public class AlunoConverter {

    public String converteParaJSON(List<Aluno> alunos) {
        JSONStringer js = new JSONStringer();
        try {

            js.object().key("List").array().object().key("aluno").array();
            for(Aluno alunoTmp : alunos) {
                js.object();
                js.key("nome").value(alunoTmp.getNome());
                js.key("nota").value(alunoTmp.getNota());
                js.endObject();
            }
            js.endArray().endObject().endArray().endObject();

        } catch (JSONException jsonException){
            Log.e("EXCEPTION", jsonException.getMessage());
        }
        return js.toString();
    }

}
