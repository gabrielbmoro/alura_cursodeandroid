package br.com.alura.agenda;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

import br.com.alura.agenda.dao.AlunoDAO;
import br.com.alura.agenda.modelo.Aluno;

public class FormularioActivity extends AppCompatActivity {

    private FormularioHelper form_helper;
    private String caminhoFoto;
    private static final int REQUEST_FOTO = 568;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        this.form_helper = new FormularioHelper(FormularioActivity.this);

        if(getIntent().getExtras()!=null) {
            Aluno aluno = (Aluno) getIntent().getExtras().getSerializable("aluno");
            if(aluno!=null) {
                this.form_helper.alimentaFormulario(aluno);
            }
        }

        Button btn_tiraFoto = (Button) findViewById(R.id.btn_tiraFoto);
        btn_tiraFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTiraFoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                caminhoFoto = getExternalFilesDir(null) + "/"+ System.currentTimeMillis() + ".jpg";
                File arquivoFoto = new File(caminhoFoto);
                intentTiraFoto.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(arquivoFoto));
                startActivityForResult(intentTiraFoto,  REQUEST_FOTO);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_FOTO) {
               form_helper.carregaImagem(caminhoFoto);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_formulario, menu);//Qual xml vamos inflar

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_formulario_ok:

                Aluno aluno = form_helper.getAluno();

                AlunoDAO dao = new AlunoDAO(this);

                if(aluno.getId()==null) {
                    dao.insere(aluno);
                    Toast.makeText(FormularioActivity.this, "O aluno(a) " + aluno.getNome() + " foi cadastrado com sucesso!", Toast.LENGTH_SHORT).show();
                }else{
                    dao.altera(aluno);
                    Toast.makeText(FormularioActivity.this, "O aluno(a) " + aluno.getNome() + " foi atualizado com sucesso!", Toast.LENGTH_SHORT).show();
                }

                dao.close();

                finish();//não volta, evita pilha de activities
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
