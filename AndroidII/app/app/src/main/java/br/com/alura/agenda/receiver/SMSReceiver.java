package br.com.alura.agenda.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.telephony.SmsMessage;
import android.widget.Toast;

import br.com.alura.agenda.R;
import br.com.alura.agenda.dao.AlunoDAO;

/**
 * Created by gabriel-moro on 07/03/17.
 */

public class SMSReceiver extends BroadcastReceiver{

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onReceive(Context context, Intent intent) {

        Object[] pdus = (Object[]) intent.getSerializableExtra("pdus");
        byte[] pdu = (byte[]) pdus[0];
        String formato = (String) intent.getSerializableExtra("format");
        SmsMessage sms = SmsMessage.createFromPdu(pdu, formato);

        String telefone = sms.getDisplayOriginatingAddress();

        AlunoDAO alunoDAO = new AlunoDAO(context);
        if(alunoDAO.ehAluno(telefone)) {
            Toast.makeText(context, "Chegou um sms!", Toast.LENGTH_SHORT).show();
            /*Pra adicionar um alerta sonoro personalizado a mensagem*/
            MediaPlayer m = MediaPlayer.create(context, R.raw.msg_sound);
            m.start();
        }
        alunoDAO.close();
    }

}
