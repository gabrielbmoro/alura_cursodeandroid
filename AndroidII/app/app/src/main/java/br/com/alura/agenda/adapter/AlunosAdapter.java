package br.com.alura.agenda.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import br.com.alura.agenda.ListaAlunosActivity;
import br.com.alura.agenda.R;
import br.com.alura.agenda.modelo.Aluno;

/**
 * Created by gabriel-moro on 06/03/17.
 */

public class AlunosAdapter extends BaseAdapter{

    private final List<Aluno> alunos;
    private final Context context;

    public AlunosAdapter(Context context, List<Aluno> alunos) {
        this.alunos = alunos;
        this.context = context;
    }

    @Override
    public int getCount() {
        return alunos.size();
    }

    @Override
    public Object getItem(int position) {
        return alunos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return alunos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Aluno aluno = alunos.get(position);

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = convertView;
        if(view==null) {
            view = inflater.inflate(R.layout.lista_item, parent, false); //para que não colocoque mais uma vez dentro da lista
        }

        TextView txtNome = (TextView) view.findViewById(R.id.item_name);
        txtNome.setText(alunos.get(position).getNome());

        TextView txtTelefone = (TextView) view.findViewById(R.id.item_telefone);
        txtTelefone.setText(alunos.get(position).getTelefone());

        ImageView img = (ImageView) view.findViewById(R.id.item_foto);
        String caminhoDaFoto = alunos.get(position).getCaminhoFoto();
        if(caminhoDaFoto!=null) {
            Bitmap bitmap = BitmapFactory.decodeFile(alunos.get(position).getCaminhoFoto());
            Bitmap bitmapReduzida = Bitmap.createScaledBitmap(bitmap, 100, 100, true);
            img.setImageBitmap(bitmapReduzida);
            img.setScaleType(ImageView.ScaleType.FIT_XY);
        }

        return view;
    }
}
