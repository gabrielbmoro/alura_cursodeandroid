package br.com.alura.agenda.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by gabrielbmoro on 04/03/17.
 */

public interface DAO {

    String DATABASE_NAME = "agenda";

    void insere(Serializable object);

    List busca();

    void deletar(Serializable object);

    void altera(Serializable object);

}
