package alura.curso.ichat_alura.callback;

import android.content.Context;
import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import alura.curso.ichat_alura.activity.MainActivity;
import alura.curso.ichat_alura.app.ChatApplication;
import alura.curso.ichat_alura.model.Mensagem;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gabriel-moro on 23/02/17.
 */

public class OuvirMensagensCallback implements Callback<Mensagem>{

    private Context context;
    private EventBus eventBus;

    public OuvirMensagensCallback(Context contextT, EventBus eventBusT){
        this.context = contextT;
        this.eventBus = eventBusT;
    }

    @Override
    public void onResponse(Call<Mensagem> call, Response<Mensagem> response) {
        if(response.isSuccessful()) {
            Mensagem mensagemRecebida = response.body();

            /*Toda vez que chamar esse método, logo em seguida serão executados
            os métodos anotados com @Subscribe, os quais são responsáveis pela
            atualização da interface gráfica com as mensagens.
             */
            eventBus.post(new MensagemEvent(mensagemRecebida));
        }
    }

    @Override
    public void onFailure(Call<Mensagem> call, Throwable t) {
        eventBus.post(new FailureEvent());
    }

}
