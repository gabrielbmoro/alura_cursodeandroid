package alura.curso.ichat_alura.model;

import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import org.json.JSONObject;
import org.json.JSONStringer;
import java.net.URL;
import java.util.Scanner;

import alura.curso.ichat_alura.activity.MainActivity;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by gabriel-moro on 22/02/17.
 */

/*Serviço vai ser implementado pelo retrofit, toda vez que executar o método enviar por exemplo,
* será disparado uma requisição do tipo POST.*/

public interface ChatService {

    @POST("polling")
    Call<Void> enviar(@Body final Mensagem mensagem);

    @GET("polling")
    Call<Mensagem> ouvirMensagem();


}
