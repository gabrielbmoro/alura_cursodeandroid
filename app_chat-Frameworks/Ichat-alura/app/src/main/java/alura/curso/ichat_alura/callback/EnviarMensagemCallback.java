package alura.curso.ichat_alura.callback;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by gabriel-moro on 23/02/17.
 */
public class EnviarMensagemCallback implements retrofit2.Callback<Void> {
    @Override
    public void onResponse(Call<Void> call, Response<Void> response) {

    }

    @Override
    public void onFailure(Call<Void> call, Throwable t) {

    }
}
