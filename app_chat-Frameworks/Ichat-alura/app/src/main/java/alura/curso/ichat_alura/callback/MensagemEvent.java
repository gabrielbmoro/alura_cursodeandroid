package alura.curso.ichat_alura.callback;

import alura.curso.ichat_alura.model.Mensagem;

/**
 * Created by gabrielbmoro on 23/02/17.
 */
public class MensagemEvent {

    public Mensagem mensagemRecebida;

    public MensagemEvent() {}
    public MensagemEvent(Mensagem mensagemX) {
        this.mensagemRecebida = mensagemX;
    }

}
