package alura.curso.ichat_alura.app;

import android.app.Application;
import alura.curso.ichat_alura.dagger.component.ChatComponent;
import alura.curso.ichat_alura.dagger.component.DaggerChatComponent;
import alura.curso.ichat_alura.dagger.module.ChatModule;

/**
 * Created by gabrielbmoro on 23/02/17.
 */

public class ChatApplication extends Application {

    private ChatComponent chatComponent;

    @Override
    public void onCreate() {
        //Com instancia personalizada
        chatComponent = DaggerChatComponent.builder()
                .chatModule(new ChatModule(this)) //chatModule criado por construtor personalizado
                .build();
    }

    public ChatComponent getChatComponent() {
        return this.chatComponent;
    }

}