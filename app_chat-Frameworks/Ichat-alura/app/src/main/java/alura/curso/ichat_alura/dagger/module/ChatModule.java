package alura.curso.ichat_alura.dagger.module;

import android.app.Application;
import android.view.inputmethod.InputMethodManager;

import org.greenrobot.eventbus.EventBus;
import com.squareup.picasso.Picasso;
import alura.curso.ichat_alura.model.ChatService;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by gabrielbmoro on 23/02/17.
 */

@Module
public class ChatModule {

    private Application app;

    public ChatModule(Application appX){
        this.app = appX;
    }

    @Provides
    public ChatService getChatService(){
        return new Retrofit.Builder()
                .baseUrl("http://192.168.25.6:8080/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ChatService.class);
    }

    @Provides
    public Picasso getPicasso(){
        return new Picasso.Builder(app).build();
    }

    @Provides
    public EventBus getEventBus() {return EventBus.builder().build();}

    @Provides
    public InputMethodManager getInputMethodManager(){ return ((InputMethodManager) app.getSystemService(INPUT_METHOD_SERVICE));}
}
