package alura.curso.ichat_alura.dagger.component;

import alura.curso.ichat_alura.activity.MainActivity;
import alura.curso.ichat_alura.adapter.MensagemAdapter;
import alura.curso.ichat_alura.callback.OuvirMensagensCallback;
import alura.curso.ichat_alura.dagger.module.ChatModule;
import dagger.Component;

/**
 * Created by gabrielbmoro on 23/02/17.
 */

@Component(modules=ChatModule.class)
public interface ChatComponent {

    //Dagger quero que tu injete em MainActivity
    void inject(MainActivity activity);
    void inject(MensagemAdapter mensagemAdapter);

}
