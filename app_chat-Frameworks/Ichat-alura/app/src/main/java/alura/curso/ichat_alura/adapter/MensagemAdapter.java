package alura.curso.ichat_alura.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;
import java.util.List;

import javax.inject.Inject;

import alura.curso.ichat_alura.R;
import alura.curso.ichat_alura.app.ChatApplication;
import alura.curso.ichat_alura.model.Mensagem;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by gabriel-moro on 22/02/17.
 *
 * Adapter no Android é uma classe que permite manipular
 * linha por linha do Componente ListView. Cada ListView possui um adaptador,
 * para que a Activity possa manipular seus recursos.
 */

public class MensagemAdapter extends BaseAdapter{

    private List<Mensagem> mensagens;
    private Activity myActivity;
    private int idDoCliente;

    @Inject
    Picasso picasso;
    @BindView(R.id.txt1)
    TextView texto;
    @BindView(R.id.iv_avatarMensagem)
    ImageView img;

    /*Aqui utilizamos um construtor que recebe uma lista de mensagens,
    * uma Activity para que possamos inflar o objeto View (permitindo a
    * manipulação linha a linha) e o ID do cliente para compararmos se a
    * mensagem é enviada pelo mesmo usuário.*/
    public MensagemAdapter(List<Mensagem> mensagens, Activity mainActivity, int idDoCliente) {
        this.mensagens = mensagens;
        this.myActivity = mainActivity;
        this.idDoCliente = idDoCliente;
    }

    @Override
    public int getCount() {
        return mensagens.size();
    }

    @Override
    public Object getItem(int position) {
        return mensagens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /*O método getView permite manipular determinada linha do componente ListView.
    * A partir do getLayoutInflater().inflate. Vale lembrar que, cada linha da listView é
    * um TextView.*/

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //A View representa toda a superfícia da tela de uma activity
        View linha = this.myActivity.getLayoutInflater().inflate(R.layout.mensagem, parent, false);
        ButterKnife.bind(this, linha);
        ((ChatApplication) this.myActivity.getApplication()).getChatComponent().inject(this);

        Mensagem mensagem = (Mensagem) getItem(position);


        picasso.with(myActivity)
                .load("https://api.adorable.io/avatars/285/"+mensagem.getId()+".png")
                .into(img);


        if(idDoCliente != mensagem.getId()) {
            linha.setBackgroundColor(Color.CYAN);
        }

        texto.setText(mensagem.getTexto());
        return linha;
    }
}
