package alura.curso.ichat_alura.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by gabriel-moro on 22/02/17.
 */
public class Mensagem implements Serializable{

    @SerializedName("text")
    private String texto;
    private int id;

    public Mensagem(int i, String s) {
        this.id = i;
        this.texto= s;
    }

    public String getTexto() {
        return texto;
    }

    public int getId() {
        return id;
    }
}
