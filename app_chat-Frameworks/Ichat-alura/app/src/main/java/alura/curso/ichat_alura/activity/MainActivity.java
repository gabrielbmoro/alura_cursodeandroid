package alura.curso.ichat_alura.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import com.squareup.picasso.Picasso;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import alura.curso.ichat_alura.app.ChatApplication;
import alura.curso.ichat_alura.R;
import alura.curso.ichat_alura.adapter.MensagemAdapter;
import alura.curso.ichat_alura.callback.EnviarMensagemCallback;
import alura.curso.ichat_alura.callback.FailureEvent;
import alura.curso.ichat_alura.callback.MensagemEvent;
import alura.curso.ichat_alura.callback.OuvirMensagensCallback;
import alura.curso.ichat_alura.model.ChatService;
import alura.curso.ichat_alura.model.Mensagem;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private int idDoCliente = 1;
    private List<Mensagem> mensagens;
    private MensagemAdapter adapter;

    @BindView(R.id.listView)
    ListView listViewElement;
    @BindView(R.id.btn_enviar)
    Button btn_enviar;
    @BindView(R.id.txt_msg)
    EditText editText_msg;
    @BindView(R.id.iv_avatarDeUser)
    ImageView imgView;

    //Dagger vai inserir tal instância, dagger não consegue injetar em atributos privados
    @Inject
    ChatService chatService;
    @Inject
    Picasso picasso;
    @Inject
    EventBus eventBus;
    @Inject
    InputMethodManager inputMethodManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ((ChatApplication) getApplication()).getChatComponent().inject(this);


        //recuperando as mensagens, caso haja alguma
        if (savedInstanceState != null) {
            mensagens = (List<Mensagem>) savedInstanceState.getSerializable("mensagens");
        } else {
            mensagens = new ArrayList<>();
        }

        picasso.with(this)
                .load("https://api.adorable.io/avatars/285/" + idDoCliente + ".png")
                .into(imgView);

        /*mensagens  = Arrays.asList(new Mensagem(1,"Olá alunos de Android!"),
                new Mensagem(2,"Olá"));*/

        adapter = new MensagemAdapter(mensagens,this,idDoCliente);

        listViewElement.setAdapter(adapter);

        ouvirMensagem(new MensagemEvent());

        eventBus.register(this);

    }

    @OnClick(R.id.btn_enviar)
    public void click(){
        chatService.enviar(new Mensagem(idDoCliente,editText_msg.getText().toString()))
                .enqueue(new EnviarMensagemCallback());
        editText_msg.getText().clear();

        /*Escondendo o teclado*/
        inputMethodManager.hideSoftInputFromWindow(editText_msg.getWindowToken(),0);
    }

    @Subscribe
    public void colocaNaLista(MensagemEvent eventoDeMensagem){
        mensagens.add(eventoDeMensagem.mensagemRecebida);
        MensagemAdapter adapter = new MensagemAdapter(mensagens, this, idDoCliente);
        listViewElement.setAdapter(adapter);
    }

    @Subscribe
    public void ouvirMensagem(MensagemEvent eventoDeMensagem){
        chatService.ouvirMensagem()
                .enqueue(new OuvirMensagensCallback(this,eventBus));
    }

    @Subscribe
    public void lidarCom(FailureEvent event) {
        ouvirMensagem(null);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("mensagens", (ArrayList<Mensagem>) mensagens);
    }

    @Override
    public void onStop(){
        eventBus.unregister(this);
    }
}
