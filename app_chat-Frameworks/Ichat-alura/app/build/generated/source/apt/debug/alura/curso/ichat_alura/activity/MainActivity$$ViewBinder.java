// Generated code from Butter Knife. Do not modify!
package alura.curso.ichat_alura.activity;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class MainActivity$$ViewBinder<T extends MainActivity> implements ViewBinder<T> {
  @Override
  public Unbinder bind(final Finder finder, final T target, Object source) {
    InnerUnbinder unbinder = createUnbinder(target);
    View view;
    view = finder.findRequiredView(source, 2131427415, "field 'listViewElement'");
    target.listViewElement = finder.castView(view, 2131427415, "field 'listViewElement'");
    view = finder.findRequiredView(source, 2131427418, "field 'btn_enviar' and method 'click'");
    target.btn_enviar = finder.castView(view, 2131427418, "field 'btn_enviar'");
    unbinder.view2131427418 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.click();
      }
    });
    view = finder.findRequiredView(source, 2131427417, "field 'editText_msg'");
    target.editText_msg = finder.castView(view, 2131427417, "field 'editText_msg'");
    view = finder.findRequiredView(source, 2131427416, "field 'imgView'");
    target.imgView = finder.castView(view, 2131427416, "field 'imgView'");
    return unbinder;
  }

  protected InnerUnbinder<T> createUnbinder(T target) {
    return new InnerUnbinder(target);
  }

  protected static class InnerUnbinder<T extends MainActivity> implements Unbinder {
    private T target;

    View view2131427418;

    protected InnerUnbinder(T target) {
      this.target = target;
    }

    @Override
    public final void unbind() {
      if (target == null) throw new IllegalStateException("Bindings already cleared.");
      unbind(target);
      target = null;
    }

    protected void unbind(T target) {
      target.listViewElement = null;
      view2131427418.setOnClickListener(null);
      target.btn_enviar = null;
      target.editText_msg = null;
      target.imgView = null;
    }
  }
}
