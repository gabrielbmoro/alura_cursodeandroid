package alura.curso.ichat_alura.dagger.component;

import alura.curso.ichat_alura.activity.MainActivity;
import alura.curso.ichat_alura.activity.MainActivity_MembersInjector;
import alura.curso.ichat_alura.adapter.MensagemAdapter;
import alura.curso.ichat_alura.adapter.MensagemAdapter_MembersInjector;
import alura.curso.ichat_alura.dagger.module.ChatModule;
import alura.curso.ichat_alura.dagger.module.ChatModule_GetChatServiceFactory;
import alura.curso.ichat_alura.dagger.module.ChatModule_GetEventBusFactory;
import alura.curso.ichat_alura.dagger.module.ChatModule_GetInputMethodManagerFactory;
import alura.curso.ichat_alura.dagger.module.ChatModule_GetPicassoFactory;
import alura.curso.ichat_alura.model.ChatService;
import android.view.inputmethod.InputMethodManager;
import com.squareup.picasso.Picasso;
import dagger.MembersInjector;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import org.greenrobot.eventbus.EventBus;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerChatComponent implements ChatComponent {
  private Provider<ChatService> getChatServiceProvider;

  private Provider<Picasso> getPicassoProvider;

  private Provider<EventBus> getEventBusProvider;

  private Provider<InputMethodManager> getInputMethodManagerProvider;

  private MembersInjector<MainActivity> mainActivityMembersInjector;

  private MembersInjector<MensagemAdapter> mensagemAdapterMembersInjector;

  private DaggerChatComponent(Builder builder) {
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {

    this.getChatServiceProvider = ChatModule_GetChatServiceFactory.create(builder.chatModule);

    this.getPicassoProvider = ChatModule_GetPicassoFactory.create(builder.chatModule);

    this.getEventBusProvider = ChatModule_GetEventBusFactory.create(builder.chatModule);

    this.getInputMethodManagerProvider =
        ChatModule_GetInputMethodManagerFactory.create(builder.chatModule);

    this.mainActivityMembersInjector =
        MainActivity_MembersInjector.create(
            getChatServiceProvider,
            getPicassoProvider,
            getEventBusProvider,
            getInputMethodManagerProvider);

    this.mensagemAdapterMembersInjector =
        MensagemAdapter_MembersInjector.create(getPicassoProvider);
  }

  @Override
  public void inject(MainActivity activity) {
    mainActivityMembersInjector.injectMembers(activity);
  }

  @Override
  public void inject(MensagemAdapter mensagemAdapter) {
    mensagemAdapterMembersInjector.injectMembers(mensagemAdapter);
  }

  public static final class Builder {
    private ChatModule chatModule;

    private Builder() {}

    public ChatComponent build() {
      if (chatModule == null) {
        throw new IllegalStateException(ChatModule.class.getCanonicalName() + " must be set");
      }
      return new DaggerChatComponent(this);
    }

    public Builder chatModule(ChatModule chatModule) {
      this.chatModule = Preconditions.checkNotNull(chatModule);
      return this;
    }
  }
}
