package alura.curso.ichat_alura.dagger.module;

import alura.curso.ichat_alura.model.ChatService;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ChatModule_GetChatServiceFactory implements Factory<ChatService> {
  private final ChatModule module;

  public ChatModule_GetChatServiceFactory(ChatModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public ChatService get() {
    return Preconditions.checkNotNull(
        module.getChatService(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<ChatService> create(ChatModule module) {
    return new ChatModule_GetChatServiceFactory(module);
  }
}
