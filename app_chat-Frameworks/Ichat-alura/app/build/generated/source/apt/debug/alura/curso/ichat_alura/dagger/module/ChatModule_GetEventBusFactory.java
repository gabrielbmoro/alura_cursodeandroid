package alura.curso.ichat_alura.dagger.module;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import org.greenrobot.eventbus.EventBus;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ChatModule_GetEventBusFactory implements Factory<EventBus> {
  private final ChatModule module;

  public ChatModule_GetEventBusFactory(ChatModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public EventBus get() {
    return Preconditions.checkNotNull(
        module.getEventBus(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<EventBus> create(ChatModule module) {
    return new ChatModule_GetEventBusFactory(module);
  }
}
