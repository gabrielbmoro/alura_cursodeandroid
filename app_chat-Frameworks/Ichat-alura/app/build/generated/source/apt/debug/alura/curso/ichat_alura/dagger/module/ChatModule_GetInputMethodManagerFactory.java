package alura.curso.ichat_alura.dagger.module;

import android.view.inputmethod.InputMethodManager;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ChatModule_GetInputMethodManagerFactory implements Factory<InputMethodManager> {
  private final ChatModule module;

  public ChatModule_GetInputMethodManagerFactory(ChatModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public InputMethodManager get() {
    return Preconditions.checkNotNull(
        module.getInputMethodManager(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<InputMethodManager> create(ChatModule module) {
    return new ChatModule_GetInputMethodManagerFactory(module);
  }
}
