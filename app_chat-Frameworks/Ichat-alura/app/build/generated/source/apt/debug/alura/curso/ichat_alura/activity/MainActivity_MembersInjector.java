package alura.curso.ichat_alura.activity;

import alura.curso.ichat_alura.model.ChatService;
import android.view.inputmethod.InputMethodManager;
import com.squareup.picasso.Picasso;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;
import org.greenrobot.eventbus.EventBus;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MainActivity_MembersInjector implements MembersInjector<MainActivity> {
  private final Provider<ChatService> chatServiceProvider;

  private final Provider<Picasso> picassoProvider;

  private final Provider<EventBus> eventBusProvider;

  private final Provider<InputMethodManager> inputMethodManagerProvider;

  public MainActivity_MembersInjector(
      Provider<ChatService> chatServiceProvider,
      Provider<Picasso> picassoProvider,
      Provider<EventBus> eventBusProvider,
      Provider<InputMethodManager> inputMethodManagerProvider) {
    assert chatServiceProvider != null;
    this.chatServiceProvider = chatServiceProvider;
    assert picassoProvider != null;
    this.picassoProvider = picassoProvider;
    assert eventBusProvider != null;
    this.eventBusProvider = eventBusProvider;
    assert inputMethodManagerProvider != null;
    this.inputMethodManagerProvider = inputMethodManagerProvider;
  }

  public static MembersInjector<MainActivity> create(
      Provider<ChatService> chatServiceProvider,
      Provider<Picasso> picassoProvider,
      Provider<EventBus> eventBusProvider,
      Provider<InputMethodManager> inputMethodManagerProvider) {
    return new MainActivity_MembersInjector(
        chatServiceProvider, picassoProvider, eventBusProvider, inputMethodManagerProvider);
  }

  @Override
  public void injectMembers(MainActivity instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.chatService = chatServiceProvider.get();
    instance.picasso = picassoProvider.get();
    instance.eventBus = eventBusProvider.get();
    instance.inputMethodManager = inputMethodManagerProvider.get();
  }

  public static void injectChatService(
      MainActivity instance, Provider<ChatService> chatServiceProvider) {
    instance.chatService = chatServiceProvider.get();
  }

  public static void injectPicasso(MainActivity instance, Provider<Picasso> picassoProvider) {
    instance.picasso = picassoProvider.get();
  }

  public static void injectEventBus(MainActivity instance, Provider<EventBus> eventBusProvider) {
    instance.eventBus = eventBusProvider.get();
  }

  public static void injectInputMethodManager(
      MainActivity instance, Provider<InputMethodManager> inputMethodManagerProvider) {
    instance.inputMethodManager = inputMethodManagerProvider.get();
  }
}
