package alura.curso.ichat_alura.adapter;

import com.squareup.picasso.Picasso;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MensagemAdapter_MembersInjector implements MembersInjector<MensagemAdapter> {
  private final Provider<Picasso> picassoProvider;

  public MensagemAdapter_MembersInjector(Provider<Picasso> picassoProvider) {
    assert picassoProvider != null;
    this.picassoProvider = picassoProvider;
  }

  public static MembersInjector<MensagemAdapter> create(Provider<Picasso> picassoProvider) {
    return new MensagemAdapter_MembersInjector(picassoProvider);
  }

  @Override
  public void injectMembers(MensagemAdapter instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.picasso = picassoProvider.get();
  }

  public static void injectPicasso(MensagemAdapter instance, Provider<Picasso> picassoProvider) {
    instance.picasso = picassoProvider.get();
  }
}
