package alura.curso.ichat_alura.dagger.module;

import com.squareup.picasso.Picasso;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ChatModule_GetPicassoFactory implements Factory<Picasso> {
  private final ChatModule module;

  public ChatModule_GetPicassoFactory(ChatModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public Picasso get() {
    return Preconditions.checkNotNull(
        module.getPicasso(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<Picasso> create(ChatModule module) {
    return new ChatModule_GetPicassoFactory(module);
  }
}
