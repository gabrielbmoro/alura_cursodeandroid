// Generated code from Butter Knife. Do not modify!
package alura.curso.ichat_alura.adapter;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class MensagemAdapter$$ViewBinder<T extends MensagemAdapter> implements ViewBinder<T> {
  @Override
  public Unbinder bind(final Finder finder, final T target, Object source) {
    InnerUnbinder unbinder = createUnbinder(target);
    View view;
    view = finder.findRequiredView(source, 2131427420, "field 'texto'");
    target.texto = finder.castView(view, 2131427420, "field 'texto'");
    view = finder.findRequiredView(source, 2131427419, "field 'img'");
    target.img = finder.castView(view, 2131427419, "field 'img'");
    return unbinder;
  }

  protected InnerUnbinder<T> createUnbinder(T target) {
    return new InnerUnbinder(target);
  }

  protected static class InnerUnbinder<T extends MensagemAdapter> implements Unbinder {
    private T target;

    protected InnerUnbinder(T target) {
      this.target = target;
    }

    @Override
    public final void unbind() {
      if (target == null) throw new IllegalStateException("Bindings already cleared.");
      unbind(target);
      target = null;
    }

    protected void unbind(T target) {
      target.texto = null;
      target.img = null;
    }
  }
}
