# Usando a API

- Para inicializar api é necessário realizar a seguinte chamada: 

```sh
$ java -jar ichat-api.jar
```

- Você pode abrir vários clientes utilizando a url http://localhost:8080 e enviar mensagens.

Outra forma de utilizar essa api é realizando a linha de comando a seguir:

```sh
curl -X POST -H "content-type: application/json" -d '{"text":"olá alunos de android", "id":123}' http://localhost:8080/polling
```

- Agora vamos utilizar um exemplo de requisição **REST**, na qual ela fica presa ao servidor, aguardando uma resposta do mesmo:

```sh
curl -X GET http://localhost:8080/polling
```
